#!/usr/bin/env python

"""
MAIN: Generator for Java models

Usage:
$ ./mojamodel.py <classname> <fields>

Args:
* <classname> - Guest
* <fields> - { GuestId FirstName LastName Email MobileNumber }

Example:
$ ./mojamodel.py Invoice Guest "{ GuestId FirstName LastName Email MobileNumber }"

"""

import argparse
import sys

from functional import seq
from stringcase import snakecase


def tidygql(gql, remove_brackets=False):
    res = gql.replace("{", " { ").replace("}", " } ")

    if remove_brackets:
        res = res.replace("{", "").replace("}", "")

    return res.replace("\t", " ").replace("\n", " ").replace("  ", " ")


def generate(classname, fields, hasBuilder):
    list_of_field = tidygql(fields).split()

    #region # Templates #
    model_template = """
@ApiModel(description="<<classname>> object")
public class <<classname>> extends ModelBase {
<<list_of_class_vars>>

  //region Get/Set
<<list_of_getset>>
  //endregion

<<builder_code>>
}"""

    class_vars_template = "  private String <<var_lowered>>;"
    getset_template = """
  public String get<<var>>() {
    return this.<<var_lowered>>;
  }

  public <<classname>> set<<var>>(final String <<var_lowered>>) {
    this.<<var_lowered>> = <<var_lowered>>;
    return this;
  }
    """

    builder_code_template = """
  //region Builder
  private <<classname>>(Builder builder) {
    super(builder);
<<list_of_builder_setvar>>
  }

  public static final class Builder extends ModelBase.Builder<Builder> {
<<list_of_builder_vars>>

    private Builder() {
    }
<<list_of_builder_with_vars>>

    public <<classname>> build() {
      return new <<classname>>(this);
    }
  }
  //endregion
"""
    builder_setvar_template = "    this.<<var_lowered>> = builder.<<var_lowered>>;"
    builder_vars_template = "    private String <<var_lowered>>;"
    builder_with_vars_template = """
    public Builder with<<var>>(String <<var_lowered>>) {
      this.<<var_lowered>> = <<var_lowered>>;
      return this;
    }"""
    #endregion


    seq_fields = (seq(list_of_field)
        .map(lambda x: x.strip())
        .filter(lambda x: "{" not in x)
        .filter(lambda x: "}" not in x)
        .map(lambda x: x)
    )

    list_of_class_vars = (seq_fields
        .map(lambda x: class_vars_template
            .replace("<<var_lowered>>", x[0].lower() + x[1:])
        )
        .to_list()
    )

    list_of_getset = (seq_fields
        .map(lambda x: getset_template
            .replace("<<classname>>", classname)
            .replace("<<var>>", x)
            .replace("<<var_lowered>>", x[0].lower() + x[1:])
        )
        .to_list()
    )

    builder_code = ""

    #region BUILDER
    if hasBuilder:
        list_of_builder_setvar = (seq_fields
            .map(lambda x: builder_setvar_template
                .replace("<<var_lowered>>", x[0].lower() + x[1:])
            )
            .to_list()
        )

        list_of_builder_vars = (seq_fields
            .map(lambda x: builder_vars_template
                .replace("<<var_lowered>>", x[0].lower() + x[1:])
            )
            .to_list()
        )

        list_of_builder_with_vars = (seq_fields
            .map(lambda x: builder_with_vars_template
                .replace("<<var>>", x)
                .replace("<<var_lowered>>", x[0].lower() + x[1:])
            )
            .to_list()
        )

        builder_code = (builder_code_template
            .replace("<<classname>>", classname)
            .replace("<<list_of_builder_setvar>>", "\n".join(list_of_builder_setvar))
            .replace("<<list_of_builder_vars>>", "\n".join(list_of_builder_vars))
            .replace("<<list_of_builder_with_vars>>", "\n".join(list_of_builder_with_vars))
        )

    #endregion


    model_code = (model_template
        .replace("<<classname>>", classname)
        .replace("<<list_of_class_vars>>", "\n".join(list_of_class_vars))
        .replace("<<list_of_getset>>", "\n".join(list_of_getset))
        .replace("<<builder_code>>", builder_code)
    )


    print "#############"
    print "### Model ###"
    print "#############"
    print(model_code)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("classname", type=str, help="Classname")
    parser.add_argument("fields", type=str, help="GraphQL to model")
    parser.add_argument("-b", "--builder", action='store_true')
    
    args = parser.parse_args()

    generate(args.classname, args.fields, args.builder)

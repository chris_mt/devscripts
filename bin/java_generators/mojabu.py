#!/usr/bin/env python

"""
Builder generator

$ ./mojabu.py Booking Search

"""

import argparse
import sys

from functional import seq
from stringcase import snakecase


def main(module, endpoint):

    #region Template
    builder_template = """
// Add this as interface
// String query<<endpoint>>(final <<module>><<endpoint>>Request request);


@Override
public String query<<endpoint>>(final <<module>><<endpoint>>Request request) {
  String response = "";

  SelectionSetBuilder builder = createIdAwareBuilder();
  List<Consumer<FieldBuilder>> consumers = new ArrayList<>();
  // consumers.add(countResults()); // if has offset/limit
  
  //region 1 Map
  // builder.withField(GqlBooking.FIELD_REFERENCE_BOOKING_ID)
  //    .withField(GqlBooking.FIELD_BOOKING_STATUS, createStatusSelectionSet());

  // if (Objects.isNull(request.getPropertyID())) {
  //   builder.withField(GqlBooking.FIELD_BOOKED_PROPERTY, createPropertyListingSummarySelectionSet());
  // } else {
  //   builder.withField(
  //     GqlBooking.FIELD_BOOKED_PROPERTY,
  //     createPropertyListingSummarySelectionSet(),
  //     eqAny(GqlPropertyListing.FIELD_ID, toIriString(request.getPropertyID())));
  // }
  //endregion

  //region 2 Filter
  // if (!Objects.isNull(request.getBookingID())) {
  //   consumers.add(fieldBuilder -> fieldBuilder.withIncludeDirective(
  //     eqAny(GqlBooking.FIELD_REFERENCE_BOOKING_ID, request.getBookingID())
  //   ));
  // }

  // if (!Objects.isNull(request.getBookingDate())) {
  //   consumers.add(fieldBuilder -> fieldBuilder.withIncludeDirective(eq(GqlBooking.FIELD_CREATED_TIMESTAMP, request.getBookingDate())));
  // }
  //endregion

  //region 2.1 Filter-Paging - if has offset/limit
  // if (!Objects.isNull(request.getLimit()) || !Objects.isNull(request.getOffset())) {
  //   consumers.add(searchWithPaginationConsumer(request.getLimit(), request.getOffset()));
  // }
  //endregion

  //region 2.2 Filter-Sorting - if has orderBy
  // if (!Objects.isNull(request.getOrderBy())) {
  //   consumers.add(sortingConsumer(SortingField.asc(request.getOrderBy())));
  // }
  //endregion

  //region 3   
  response = createQuery(builder.build(), consumers);
  //endregion

  return response;
}
    """
    #endregion

    builder_code = (builder_template
        .replace("<<module>>", module)
        .replace("<<module_upper>>", module.upper())
        .replace("<<endpoint>>", endpoint)
        .replace("<<endpoint_lowered>>", endpoint[0].lower() + endpoint[1:])
    )

    print "#######################"
    print "### Util - Builders ###"
    print "#######################"
    print(builder_code)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("module", type=str, help="Module")
    parser.add_argument("endpoint", type=str, help="Endpoint")
    args = parser.parse_args()

    main(args.module, args.endpoint)

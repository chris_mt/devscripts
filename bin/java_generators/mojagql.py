#!/usr/bin/env python

"""
Builder generator

$ ./mojagql.py Transaction "{ Date Description DiscountAmount IsReconciled Narration Reference SubTotal TaxTotal Total TotalOutstanding TotalPaid TransactionID XeroCode }"
"""

import argparse
import sys

from functional import seq
from stringcase import snakecase

def main(classname, fields):
    seq_fields = seq(fields.split()).filter(lambda x: x not in ["{", "}"])

    #region static
    static_template = '  public final static String FIELD_<<field_snake_upper>> = "<<field>>";'

    static_list = (seq_fields
        .map(lambda x: static_template
            .replace("<<field>>", x)
            .replace("<<field_snake_upper>>", snakecase(x).upper())
        )
        .to_list()
    )
    static_code = "\n".join(static_list)
    #endregion

    #region vars
    vars_template = '  private String <<field_camel>>;'
    vars_list = (seq_fields
        .map(lambda x: vars_template
            .replace("<<field_camel>>", x[0].lower() + x[1:])
        )
        .to_list()
    )
    vars_code = "\n".join(vars_list)
    #endregion

    #region getset
    getset_template = """
  public String get<<field>>() {
    return <<field_camel>>;
  }

  public Gql<<classname>> set<<field>>(final String <<field_camel>>) {
    this.<<field_camel>> = <<field_camel>>;
    return this;
  }
    """
    getset_list = (seq_fields
        .map(lambda x: getset_template
            .replace("<<classname>>", classname)
            .replace("<<field>>", x)
            .replace("<<field_camel>>", x[0].lower() + x[1:])
        )
        .to_list()
    )
    getset_code = "\n".join(getset_list)
    #endregion

    #region gqlclass
    class_template = """
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Gql<<classname>> extends GqlBase {

  public final static String TYPENAME = "<<classname>>";

<<static_code>>

<<vars_code>>

  public Gql<<classname>>() {
    this.setTypename(TYPENAME);
  }

  //region Overrides
  @Override
  public String getEntityTypeName() {
    return TYPENAME;
  }

  //endregion

  //region Get/Set
  public Gql<<classname>> asObjectReference() {
    return new Gql<<classname>>() {{
      setId(this.getId());
    }};
  }

<<getset_code>>
  //endregion
}
    """

    class_code = (class_template
        .replace("<<classname>>", classname)
        .replace("<<static_code>>", static_code)
        .replace("<<vars_code>>", vars_code)
        .replace("<<getset_code>>", getset_code)
    )
    #endregion

    print "########################"
    print "### Util - Gql Class ###"
    print "########################"
    print(class_code)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("classname", type=str, help='ie "Transaction"')
    parser.add_argument("fields", type=str, help='ie "{ Date Description SubTotal TaxTotal Total }"')
    args = parser.parse_args()

    main(args.classname, args.fields)

#!/usr/bin/env python

"""
Resource/Init generator

$ ./mojare.py Booking

"""

import argparse
import sys

from functional import seq
from stringcase import snakecase


def main(module):
    #region Templates
    #-------------------- Interfaces --------------------
    interface_template = """
public interface <<module>>Repository {
}

"""

    #-------------------- Implementation --------------------
    implementation_template = """
@Singleton
public class <<module>>RepositoryImpl implements <<module>>Repository {
  private static final Logger LOG = LoggerFactory.getLogger(<<module>>RepositoryImpl.class);
  private static final ZoneId DEFAULT_TIME_ZONE_ID = ZoneId.of("Pacific/Auckland");

  private final <<module>>QueryBuilder builder;

  @Inject
  public <<module>>RepositoryImpl(
    <<module>>QueryBuilder builder
  ) {
    this.builder = builder;
  }
}

"""

    #-------------------- Resource --------------------
    resource_template = """
@Api(value = ""###REPLACE_THIS###")
@Path("/v1/<<module_lower>>")
@Produces(APPLICATION_JSON)
public class <<module>>Resource {
  private static final Logger LOG = LoggerFactory.getLogger(<<module>>Resource.class);
  private static final String TAG = "<<module>>";

  private final <<module>>ServiceConfig config;
  private final <<module>>Repository repository;

  @Inject
  public <<module>>Resource(final <<module>>ServiceConfig <<module_lower>>ServiceConfig,
                         final <<module>>Repository repository) {
    this.config = <<module_lower>>ServiceConfig;
    this.repository = repository;
  }

}
"""

    application_template = """
>>> Add this line in <<module>>ServiceApplication.java <<<

bootstrap.addBundle(GuiceBundle.builder()
  .extensions(
    <<module>>Resource.class
  )
.build();
"""

    module_template = """
>>> Add this line in <<module>>ServiceModule.java <<<

bind(<<module>>Repository.class).to(<<module>>RepositoryImpl.class);
"""

    builder_interface_template = """
public interface <<module>>QueryBuilder extends QueryBuilder<Gql---object-here---> {
}
"""

    builder_implementation_template = """
@Singleton
public class <<module>>QueryBuilderImpl extends QueryBuilderImpl<Gql---object-here---> implements <<module>>QueryBuilder {
  private static final Logger LOG = LoggerFactory.getLogger(<<module>>QueryBuilderImpl.class);

  @Inject
  public <<module>>QueryBuilderImpl(final GraphQLClient client) {
    super(client);
  }

  //region Base overrides
  @Override
  public String getEntityTypeName() {
    return Gql---object-here---.TYPENAME;
  }

  @Override
  public TypeReference<Gql---object-here---> getTypeReference() {
    return new TypeReference<Gql---object-here--->() {};
  }

  @Override
  public SelectionSet buildEntitySelectionSet(@Deprecated boolean detail) {
    return createIdAwareBuilder().build();
  }
  //endregion

}

"""
    #endregion

    interface_code = (interface_template
        .replace("<<module>>", module)
    )

    implementation_code = (implementation_template
        .replace("<<module>>", module)
    )

    resource_code = (resource_template
        .replace("<<module>>", module)
        .replace("<<module_lower>>", module[0].lower() + module[1:])
    )

    application_code = (application_template
        .replace("<<module>>", module)
    )

    module_code = (module_template
        .replace("<<module>>", module)
    )

    builder_interface_code = (builder_interface_template
        .replace("<<module>>", module)
    )

    builder_implementation_code = (builder_implementation_template
        .replace("<<module>>", module)
    )

    print "#######################"
    print "### API - Interface ###"
    print "#######################"
    print(interface_code)

    print "############################"
    print "### API - Implementation ###"
    print "############################"
    print(implementation_code)

    print "######################"
    print "### API - Resource ###"
    print "######################"
    print(resource_code)

    print "##########################"
    print "### API - Application  ###"
    print "##########################"
    print(application_code)

    print "#####################"
    print "### API - Module  ###"
    print "#####################"
    print(module_code)

    print "#################################"
    print "### Util - Builder interface  ###"
    print "#################################"
    print(builder_interface_code)

    print "######################################"
    print "### Util - Builder implementation  ###"
    print "######################################"
    print(builder_implementation_code)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("module", type=str, help="Module")
    args = parser.parse_args()

    main(args.module)

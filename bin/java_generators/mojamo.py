#!/usr/bin/env python

"""
Model generator

$ ./mojamo.py Booking Search "{ Status Limit Offset OrderBy }" "{ Id Date BookingId OwnerId OwnerName PropertyId PropertyName Total Status }"

"""

import argparse
import sys

from functional import seq
from stringcase import snakecase


def tidygql(gql, remove_brackets=False):
    res = gql.replace("{", " { ").replace("}", " } ")

    if remove_brackets:
        res = res.replace("{", "").replace("}", "")

    return res.replace("\t", " ").replace("\n", " ").replace("  ", " ")


def generate_request(module, endpoint, request):

    #region # Templates #
    request_model_template = """
@ApiModel(description="<<module>> <<endpoint>> Request object")
public class <<module>><<endpoint>>Request {
<<list_of_request_class_vars>>

  //region Get/Set
  <<list_of_request_getset>>
  //endregion
}
    """

    request_class_vars_template = "  private String <<var_lowered>>;"
    request_getset_template = """
  public String get<<var>>() {
    return this.<<var_lowered>>;
  }

  public <<module>><<endpoint>>Request set<<var>>(final String <<var_lowered>>) {
    this.<<var_lowered>> = <<var_lowered>>;
    return this;
  }
    """

    #endregion

    seq_request = (seq(request)
        .map(lambda x: x.strip())
        .filter(lambda x: "{" not in x)
        .filter(lambda x: "}" not in x)
        .map(lambda x: x)
    )

    list_of_request_class_vars = (seq_request
        .map(lambda x: request_class_vars_template
            .replace("<<var_lowered>>", x[0].lower() + x[1:])
        )
        .to_list()
    )

    list_of_request_getset = (seq_request
        .map(lambda x: request_getset_template
            .replace("<<module>>", module)
            .replace("<<endpoint>>", endpoint)
            .replace("<<var>>", x)
            .replace("<<var_lowered>>", x[0].lower() + x[1:])
        )
        .to_list()
    )

    request_model_code = (request_model_template
        .replace("<<module>>", module)
        .replace("<<endpoint>>", endpoint)
        .replace("<<list_of_request_class_vars>>", "\n".join(list_of_request_class_vars))
        .replace("<<list_of_request_getset>>", "\n".join(list_of_request_getset))
    )

    print "#####################"
    print "### API - Request ###"
    print "#####################"
    print(request_model_code)


def generate_response(module, endpoint, response):

    #region # Templates #
    response_model_template = """
@ApiModel(description="<<module>> <<endpoint>> Response object")
public class <<module>><<endpoint>>Response {
<<list_of_response_class_vars>>

  //region Get/Set
<<list_of_response_getset>>
  //endregion
}
    """

    response_class_vars_template = "  private String <<var_lowered>>;"
    response_getset_template = """
  public String get<<var>>() {
    return this.<<var_lowered>>;
  }

  public <<module>><<endpoint>>Response set<<var>>(final String <<var_lowered>>) {
    this.<<var_lowered>> = <<var_lowered>>;
    return this;
  }
    """
    #endregion


    seq_response = (seq(response)
        .map(lambda x: x.strip())
        .filter(lambda x: "{" not in x)
        .filter(lambda x: "}" not in x)
        .map(lambda x: x)
    )

    list_of_response_class_vars = (seq_response
    .map(lambda x: response_class_vars_template
        .replace("<<var_lowered>>", x[0].lower() + x[1:])
    )
    .to_list()
    )

    list_of_response_getset = (seq_response
    .map(lambda x: response_getset_template
        .replace("<<module>>", module)
        .replace("<<endpoint>>", endpoint)
        .replace("<<var>>", x)
        .replace("<<var_lowered>>", x[0].lower() + x[1:])
    )
    .to_list()
    )

    response_model_code = (response_model_template
        .replace("<<module>>", module)
        .replace("<<endpoint>>", endpoint)
        .replace("<<list_of_response_class_vars>>", "\n".join(list_of_response_class_vars))
        .replace("<<list_of_response_getset>>", "\n".join(list_of_response_getset))
    )

    print "######################"
    print "### API - Response ###"
    print "######################"
    print(response_model_code)

def generate_mock(response):
    template = """
{
  "data": [
    {
<<data>>
    }
  ],
  "count": 1
}
"""
    data = (seq(response)
        .filter(lambda x: x not in ["{", "}"])
        .map(lambda x: '      "<<var>>": "<<var_data>>",'
            .replace("<<var>>", x[0].lower() + x[1:])
            .replace("<<var_data>>", x + " sample data" if "date" not in x.lower() else "2018-01-01T12:00:00Z")
        )
        .to_list()
    )

    print "#########################"
    print "### API - Mocked json ###"
    print "#########################"
    print(template.replace("<<data>>", "\n".join(data)))


def main(module, endpoint, request, response):
    request = tidygql(request).split()
    response = tidygql(response).split()

    generate_request(module, endpoint, request)
    generate_response(module, endpoint, response)
    generate_mock(response)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("module", type=str, help="Module")
    parser.add_argument("endpoint", type=str, help="Endpoint")
    parser.add_argument("request", type=str, help="GraphQL request to model")
    parser.add_argument("response", type=str, help="GraphQL response to model and mock (if needed)")
    args = parser.parse_args()

    main(args.module, args.endpoint, args.request, args.response)
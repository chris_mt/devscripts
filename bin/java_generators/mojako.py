#!/usr/bin/env python

"""
MAIN: Generator for Java API services

Usage:
$ ./mojako.py <Module> <Endpoint> <Verb> <RequestGql> <ResponseGql>

Args:
* <Module> - Booking
* <Endpoint> - Search
* <Verb> - GET
* <RequestGql> - { BookingId PropertyId GuestId BookingDate StartDate Limit Offset OrderBy }
* <ResponseGql> - { BookingId StartDate EndDate Property { PropertyId Name } Guest { GuestId FirstName LastName Email MobileNumber } }

Example:
$ ./mojako.py GET Booking Search "{ BookingId PropertyId GuestId BookingDate StartDate Limit Offset OrderBy }" "{ Id BookingId StartDate EndDate PropertyId GuestId }"

"""

import argparse

import mojare
import mojaen
import mojamo
import mojabu


parser = argparse.ArgumentParser()
parser.add_argument("verb", type=str, help="GET/POST/PUT/PATCH/DELETE")
parser.add_argument("module", type=str, help="Module")
parser.add_argument("endpoint", type=str, help="Endpoint")
parser.add_argument("request", type=str, help="Response object in GraphQL")
parser.add_argument("response", type=str, help="Response object in GraphQL")
parser.add_argument("-p", "--permission", type=str, help="READ/UPDATE/DELETE")
args = parser.parse_args()

mojamo.main(args.module, args.endpoint, args.request, args.response)
mojabu.main(args.module, args.endpoint)
mojaen.main(args.module, args.endpoint, args.verb, args.request, args.response)

print """
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Review the generated codes above.

You will need to provide the following details before you can build:
* API: Resource - codes, response codes, query params (GET)
* API: Transformer - target Gql entity, convertion of entity to model
* UTILS: Builder implementation - query codes (steps 1-3), selection sets
* If you want to override response object with Builder, use mojamodel tool

AFFECTED FILES:
===============

bachcare-utils (project)
------------------------
client/model
- XyzRequest
- XyzResponse
graphql/model
- GqlXyz
grapql/query
- XyzQueryBuilder
- XyzQueryBuilderImpl

service (project)
-----------------
transformer
- XyzTransformer
v1/dao
- XyzRepository
dao
- XyxRepositoryImpl
v1/resources
- XyzResource

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
FIN.
"""
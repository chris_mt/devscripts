# Helpful scripts for developers #


## Installation: ##

```
$ pip install -r requirements.txt
```

## Scripts: ##
1. mojako - endpoint generator

```
ie: $ ./mojako.py GET Booking Search "{ BookingId PropertyId GuestId BookingDate StartDate Limit Offset OrderBy }" "{ Id BookingId StartDate EndDate PropertyId GuestId }"
```

2. mojare - Resource generator

```
$ ./mojare.py Booking
```

3. mojagql - Gql class generator

```
$ ./mojagql.py Transaction "{ Date Description DiscountAmount IsReconciled Narration Reference SubTotal TaxTotal Total TotalOutstanding TotalPaid TransactionID XeroCode }"
```

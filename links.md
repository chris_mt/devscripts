# Links #
[Edit this page]

### Bachcare APIs ###
> Note: Just replace `dev` with `test`, `uat`, or `prod` to access env specific

Project | URL
----|----
booking-api|https://dev-api.bachcare.co.nz/booking-api/doc
email-service|https://dev-api.bachcare.co.nz/email-service/doc
property-service|https://dev-api.bachcare.co.nz/property-service/doc
supplier-api|https://dev-api.bachcare.co.nz/supplier-api/doc
finance-service|https://dev-api.bachcare.co.nz/finance-service/doc
guest-service|https://dev-api.bachcare.co.nz/guest-service/doc


----


### Sites ###
> Note: Just replace `dev` with `test`, `uat`, or `prod` to access env specific

Site|URL
----|----
JIRA Bachcare sprint|https://meaningfultechnology.atlassian.net/secure/RapidBoard.jspa?rapidView=16
Bachcare mail|https://mailcatcher.dev.bachcare.co.nz:5000
Blazegraph|http://dev-api.bachcare.co.nz/blazegraph/
Manager|https://dev-manager.bachcare.co.nz
Supplier|https://dev-supplier.bachcare.co.nz


----


### Meaningful tools ###

Site|URL
----|----
GoCD|https://gocd.bachcare.co.nz:8154/go/pipelines
AWS logs|https://ap-southeast-2.console.aws.amazon.com/cloudwatch/home?region=ap-southeast-2#logs:
AWS SQS|https://console.aws.amazon.com/sqs/home?region=us-west-2
Graphiql|http://dev-api.bachcare.co.nz/graphql-service/graphiql
Transform swagger|https://dev-api.bachcare.co.nz/transform-service/doc
Transform functions|https://dev-api.bachcare.co.nz/transform-service/v1/transform/functions
Schema service|http://dev-api.meaningful.co.nz/schema-service/v1/ui


----


[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[Chris DR]

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Chris DR]: <chris@meaningful.co.nz>
   [Edit this page]: <https://bitbucket.org/chris_mt/devscripts/src/master/links.md?mode=edit>
